import React from 'react'
import ReactDOM from 'react-dom'
import Post from './components/Post'
import dataPost from './data/dataPost'
import Tablecell from './components/Tablecell'

function MyApp() {

    const postcomp = dataPost.map(dpost => {
        return <Post key={dpost.id} title={dpost.title} id={dpost.userId} contents={dpost.body} />
    })

    const cellcomp = dataPost.map(cpost => {
        return <Tablecell key={cpost.id} id={cpost.id} title={cpost.title} contents={cpost.body} />
    })

    return <div className="row">
        <div className="col-md-8"><br /><br /><br /><br /><br />
            <table className="table">
                <tbody>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Contents</th>
                    </tr>
                </tbody>
                {cellcomp}

            </table>



        </div>
        <div className="col-md-4 ">
            <br /><br /><br /><br /><br />
            {postcomp}




        </div>
    </div>

}

//export default MyApp
ReactDOM.render(<MyApp />, document.getElementById('root'))