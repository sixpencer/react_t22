import React from 'react';

function Post(props) {
    return <div className="card" style={{ width: "18rem" }}>
        <div className="card-body">
            <h5 className="card-title">User Id  {props.id}</h5>
            <h6 className="card-subtitle mb-2 text-muted">Title : {props.title}</h6>
            <p className="card-text">{props.contents}</p>
            <a href="#" className="btn card-link btn-warning btn-sm">Card link</a>
            <a href="#" className="btn card-link btn-success btn-sm">Another link</a>
        </div>
    </div>
}

export default Post