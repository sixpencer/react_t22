import React from 'react';

function Tablecell(props) {
    return <tbody><tr>
        <td>{props.id}</td>
        <td>{props.title}</td>
        <td>{props.contents}</td>
    </tr></tbody>

}

export default Tablecell


